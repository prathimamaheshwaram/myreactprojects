import React, { Component } from 'react'
import CssBaseline from '@material-ui/core/CssBaseline'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import { Route, Switch } from 'react-router-dom'
import DashHome from './components/project/dashHome'
import Homescreen from './components/auth/homescreen'
import Signin from './components/auth/Signin'
import Signup from './components/auth/Signup'
import ForgotPassword from './components/auth/forgotpassword'

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#212121'
    },
    secondary: {
      main: '#ffa000'
    }
  },
  typography: {
    useNextVariants: true,
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"'
    ].join(','),
    h6: {
      color: 'rgba(255,255,255,0.8)' // white
    },
    body2: {
      color: 'rgba(255,255,255,0.5)' // white
    },
    overline: {
      color: 'rgba(255,255,255,0.8)' // white
    },
    body1: {
      color: 'rgba(0,0,0,0.5)', // black
      fontSize: 14
    },
    display1: {
      color: 'rgba(0,0,0,0.1)', // black
      fontSize: '-20%'
    },
    subtitle1: {
      fontSize: 16,
      color: 'rgba(0,0,0,0.65)' // black
    },
    h5: {
      color: 'rgba(0,0,0,0.65)'
    }
  }
})
class App extends Component {
  render () {
    return (
      <MuiThemeProvider theme={theme}>
        <React.Fragment>
          <CssBaseline />
          <Switch>
            <Route
              path='/'
              exact
              render={props => (
                <Homescreen {...props} component={<Signin {...props} />} />
              )}
            />
            <Route
              path='/signup'
              exact
              render={props => (
                <Homescreen {...props} component={<Signup {...props} />} />
              )}
            />
            <Route
              path='/forgotpassword'
              exact
              render={props => (
                <Homescreen
                  {...props}
                  component={<ForgotPassword {...props} />}
                />
              )}
            />

            <Route path='/homescreen/:id' component={DashHome} />
          </Switch>
        </React.Fragment>
      </MuiThemeProvider>
    )
  }
}

export default App
