import React from 'react'
import { Button, Typography } from '@material-ui/core'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import { NavLink } from 'react-router-dom'

const signin = {
  title: 'Enter correct details',
  contentText:
    'details are invalid.. Enter correct details OR signup to register your details',
  buttons: ['back', 'Sign Up'],
  navlink: '/signup'
}
const signup = {
  title: 'You are already registered',
  contentText: 'you mail id is already registered..please sign in to continue',
  buttons: ['back', 'Sign In'],
  navlink: '/'
}
const forgotpassword = {
  title: 'This mail is not registered',
  contentText:
    'the mail id is not registered in our website...sign up to register',
  buttons: ['back', 'Sign Up'],
  navlink: '/signup'
}
class Alert extends React.Component {
  state = {
    open: this.props.open
  }
  handleClose = () => {
    this.setState({ open: false })
    this.props.toggleAlert()
  }

  render () {
    const from =
      this.props.from === 'signin'
        ? signin
        : this.props.from === 'signup'
          ? signup
          : forgotpassword
    return (
      <div>
        <Dialog open={this.state.open} onClose={this.handleClose}>
          <DialogTitle disableTypography>
            <Typography variant='h5'>{from.title}</Typography>
          </DialogTitle>
          <DialogContent>
            <DialogContentText>{from.contentText}</DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color='primary'>
              {from.buttons[0]}
            </Button>
            <NavLink to={from.navlink}>
              <Button onClick={this.handleClose} color='primary' autoFocus>
                {from.buttons[1]}
              </Button>
            </NavLink>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}

export default Alert
