import React, { Component } from 'react'
import {
  FormControl,
  InputLabel,
  Input,
  InputAdornment,
  IconButton,
  FormHelperText,
  Grid,
  Typography,
  Fab
} from '@material-ui/core'
import { Visibility, VisibilityOff } from '@material-ui/icons'
import MailIcon from '@material-ui/icons/Mail'
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos'
import { NavLink } from 'react-router-dom'
import { connect } from 'react-redux'
import Alert from './Alert'
const emailtype = RegExp(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)

class Signin extends Component {
  state = {
    email: '',
    password: '',
    showPassword: false,
    err: '',
    alert: false,
    error: {
      email: 'enter your mail id',
      password: 'enter password'
    }
  }

  handleChange = prop => event => {
    this.setState({ [prop]: event.target.value })
    let error = this.state.error
    switch (prop) {
      case 'email':
        error.email = emailtype.test(event.target.value)
          ? ''
          : 'email should be in the form of example@example.com'
        break
      case 'password':
        error.password =
          event.target.value.length < 3 ? 'enter correct password' : ''
        break
      default:
        break
    }
    this.setState({ error })
  }
  handleClickShowPassword = show => {
    this.setState(state => ({ [show]: !state[show] }))
  }

  handleSubmit = () => {
    if (this.formError()) {
      const { email, password } = this.state
      this.props.signIn({ email, password })
      this.setState({ err: false })
      if (this.props.verified.signedIn === true) {
        this.props.history.push('/homescreen/' + this.props.verified.details.id)
      } else {
        this.setState({ alert: true })
      }
    } else {
      this.setState({ err: true })
    }
  }
  formError = () => {
    const error = Object.entries(this.state.error)
    let success = true
    error.map(e => e[1].length > 0 && (success = false))
    return success
  }

  toggleAlert = () => {
    this.setState({ alert: !this.state.alert })
  }

  render () {
    const { email, password, error, err, alert } = this.state

    return (
      <Grid container alignItems='flex-end' alignContent='center'>
        {alert && <Alert open={alert} toggleAlert={this.toggleAlert} from='signin'/>}
        <form noValidate>
          <Grid item xs={12} container spacing={16}>
            <Grid item xs={12}>
              <Typography variant='h5' gutterBottom>
                <span className='signinbg'> Sign In </span>
              </Typography>
            </Grid>
            <Grid item xs={12} />
            <Grid item xs={12}>
              <Typography variant='subtitle1'>We Will Need...</Typography>
            </Grid>

            <Grid item xs={12}>
              <FormControl fullWidth>
                <InputLabel htmlFor='email'>
                  <Typography variant='body1'>mail address</Typography>
                </InputLabel>
                <Input
                  id='email'
                  value={email}
                  onChange={this.handleChange('email')}
                  endAdornment={
                    <InputAdornment position='end'>
                      <MailIcon color='disabled' />
                    </InputAdornment>
                  }
                />
                {err ? (
                  <FormHelperText error>{error.email}</FormHelperText>
                ) : (
                  <FormHelperText />
                )}
              </FormControl>
            </Grid>

            <Grid item xs={12}>
              <FormControl fullWidth>
                <InputLabel htmlFor='password'>
                  <Typography variant='body1'>Your Password</Typography>
                </InputLabel>
                <Input
                  id='adornment-password'
                  type={this.state.showPassword ? 'text' : 'password'}
                  value={password}
                  onChange={this.handleChange('password')}
                  endAdornment={
                    <InputAdornment position='end'>
                      <IconButton
                        aria-label='Toggle password visibility'
                        onClick={() =>
                          this.handleClickShowPassword('showPassword')
                        }
                      >
                        {this.state.showPassword ? (
                          <Visibility />
                        ) : (
                          <VisibilityOff />
                        )}
                      </IconButton>
                    </InputAdornment>
                  }
                />
                {err ? (
                  <FormHelperText error>{error.password}</FormHelperText>
                ) : (
                  <FormHelperText />
                )}
                <Grid
                  container
                  spacing={16}
                  direction='column'
                  alignItems='flex-end'
                  wrap='nowrap'
                >
                  <Grid item xs={12}>
                    <Typography variant='body1'>
                      <NavLink to='/forgotpassword'>forgot password</NavLink>
                    </Typography>
                  </Grid>
                </Grid>
              </FormControl>
            </Grid>
            <Grid
              item
              xs={12}
              container
              spacing={16}
              direction='column'
              alignItems='center'
              wrap='nowrap'
            >
              <Grid item xs={12} />
              <Grid item xs={12} />
              <Grid item xs={12}>
                <Typography variant='body1'>
                  didn't have an Account..?
                  <NavLink to='/signup'>Sign Up</NavLink>
                </Typography>
              </Grid>
            </Grid>
          </Grid>

          <Grid item container justify='center'>
            <Fab
              color='primary'
              style={{ position: 'absolute', bottom: '1%' }}
              onClick={this.handleSubmit}
            >
              <ArrowForwardIosIcon />
            </Fab>
          </Grid>
        </form>
      </Grid>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    signIn: signinDetails => {
      dispatch({ type: 'signIn', signinDetails })
    }
  }
}
const mapStateToProps = state => {
  return {
    verified: state.auth.auth
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Signin)
