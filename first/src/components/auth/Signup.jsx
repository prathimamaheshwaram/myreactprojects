import React, { Component } from 'react'
import {
  FormControl,
  InputLabel,
  Input,
  InputAdornment,
  IconButton,
  Button,
  FormHelperText,
  Grid,
  Typography,
  Fab
} from '@material-ui/core'
import { SocialIcon } from 'react-social-icons'
import { Visibility, VisibilityOff } from '@material-ui/icons'
import MailIcon from '@material-ui/icons/Mail'
import InsertEmoticonIcon from '@material-ui/icons/InsertEmoticon'
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos'
import { connect } from 'react-redux'
import Alert from './Alert'
const emailtype = RegExp(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)

class Signup extends Component {
  state = {
    firstName: '',
    email: '',
    password: '',
    confirmPassword: '',
    showPassword: false,
    showConfirmPassword: false,
    alert: false,
    err: '',
    error: {
      firstName: 'enter first name',
      confirmPassword: 'confirm password',
      email: 'enter your mail id',
      password: 'enter password'
    }
  }

  handleChange = prop => event => {
    this.setState({ [prop]: event.target.value })
    let error = this.state.error
    switch (prop) {
      case 'firstName':
        error.firstName =
          event.target.value.length < 3
            ? 'first Name should be greate than 3'
            : ''
        break
      case 'confirmPassword':
        error.confirmPassword =
          event.target.value.length < 3 ? 'password should be greater than 3 characters' : ''
        break
      case 'email':
        error.email = emailtype.test(event.target.value)
          ? ''
          : 'email should be in the form of example@example.com'
        break
      case 'password':
        error.password =
          event.target.value.length < 3 ? 'password should be greater than 3 characters' : ''
        break
      default:
        break
    }
    this.setState({ error })
  }
  handleClickShowPassword = show => {
    this.setState(state => ({ [show]: !state[show] }))
  }
  toggleAlert = () => {
    this.setState({ alert: !this.state.alert })
  }
  handleSubmit = () => {
    if (
      this.formError() &&
      (this.state.password === this.state.confirmPassword)
    ) {
      const { firstName, email, password } = this.state
      this.props.addUser({ firstName, email, password })
      this.setState({ err: false })
      if (this.props.verified.signedUp === true) {
        this.props.history.push('/homescreen/' + this.props.verified.details.id)
      }
      else if(this.props.verified.alreadyExist){
        this.setState({ alert: true })
      }
    } else {
      let error=this.state.error;
      if(this.state.password!==this.state.confirmPassword){
      error.confirmPassword='passwords should match'
      }
      this.setState({ err: true,error})
    }
  }
  formError = () => {
    const error = Object.entries(this.state.error)
    let success = true
    error.map(e => 
      (e[1].length > 0) &&(success = false)
   )
    return success
  }

  render () {
    const { confirmPassword, error, err,alert } = this.state
    const { firstName, email, password } = this.state

    return (
      <Grid
        container
        wrap='wrap'
        direction='column'
        spacing={32}
        justify='space-between'
        alignItems='stretch'
      >
       {alert && <Alert open={alert} toggleAlert={this.toggleAlert}  from='signup' />}
        <Grid item xs zeroMinWidth>
          <Typography variant='h5' gutterBottom>
            <span className='signinbg'> Sign Up </span>
          </Typography>
        </Grid>
        <form noValidate>
          <Typography variant='subtitle1'>We Will Need...</Typography>
          <Grid
            item
            xs={12}
            container
            wrap='wrap'
            alignItems='stretch'
            justify='space-between'
          >
            <Grid
              item
              xs={8}
              container
              direction='column'
              justify='space-between'
              alignItems='stretch'
              spacing={8}
              wrap='nowrap'
            >
              <Grid item zeroMinWidth xs>
                <FormControl fullWidth>
                  <InputLabel htmlFor='firstName'>
                    <Typography variant='body1'>Your Name</Typography>
                  </InputLabel>
                  <Input
                    id='firstName'
                    value={firstName}
                    onChange={this.handleChange('firstName')}
                    endAdornment={
                      <InputAdornment position='end'>
                        <InsertEmoticonIcon color='disabled' />
                      </InputAdornment>
                    }
                  />

                  {err ? (
                    <FormHelperText error>{error.firstName}</FormHelperText>
                  ) : (
                    <FormHelperText />
                  )}
                </FormControl>
              </Grid>
              <Grid item zeroMinWidth xs>
                <FormControl fullWidth>
                  <InputLabel htmlFor='email'>
                    <Typography variant='body1'>mail address</Typography>
                  </InputLabel>
                  <Input
                    id='email'
                    value={email}
                    onChange={this.handleChange('email')}
                    endAdornment={
                      <InputAdornment position='end'>
                        <MailIcon color='disabled' />
                      </InputAdornment>
                    }
                  />
                  {err ? (
                    <FormHelperText error>{error.email}</FormHelperText>
                  ) : (
                    <FormHelperText />
                  )}
                </FormControl>
              </Grid>
              <Grid item zeroMinWidth xs>
                <FormControl fullWidth>
                  <InputLabel htmlFor='password'>
                    <Typography variant='body1'>Your Password</Typography>
                  </InputLabel>
                  <Input
                    id='adornment-password'
                    type={this.state.showPassword ? 'text' : 'password'}
                    value={password}
                    onChange={this.handleChange('password')}
                    endAdornment={
                      <InputAdornment position='end'>
                        <IconButton
                          aria-label='Toggle password visibility'
                          onClick={() =>
                            this.handleClickShowPassword('showPassword')
                          }
                        >
                          {this.state.showPassword ? (
                            <Visibility />
                          ) : (
                            <VisibilityOff />
                          )}
                        </IconButton>
                      </InputAdornment>
                    }
                  />
                  {err ? (
                    <FormHelperText error>{error.password}</FormHelperText>
                  ) : (
                    <FormHelperText />
                  )}
                </FormControl>
              </Grid>
              <Grid item zeroMinWidth xs>
                <FormControl fullWidth>
                  <InputLabel htmlFor='confirmPassword'>
                    <Typography variant='body1'>One More Time ?</Typography>
                  </InputLabel>
                  <Input
                    id='adornment-confirmPassword'
                    type={this.state.showConfirmPassword ? 'text' : 'password'}
                    value={confirmPassword}
                    onChange={this.handleChange('confirmPassword')}
                    endAdornment={
                      <InputAdornment position='end'>
                        <IconButton
                          aria-label='Toggle password visibility'
                          onClick={() =>
                            this.handleClickShowPassword('showConfirmPassword')
                          }
                        >
                          {this.state.showConfirmPassword ? (
                            <Visibility />
                          ) : (
                            <VisibilityOff />
                          )}
                        </IconButton>
                      </InputAdornment>
                    }
                  />

                  {err ? (
                    <FormHelperText error>
                      {error.confirmPassword}
                    </FormHelperText>
                  ) : (
                    <FormHelperText />
                  )}
                </FormControl>
              </Grid>
            </Grid>
            <Grid item xs={1}>
              <hr width='1' size='110' />
              <Typography variant='subtitle1'>OR</Typography>
              <hr width='1' size='110' />
            </Grid>
            <Grid
              item
              xs={3}
              container
              direction='column'
              alignItems='stretch'
              justify='space-between'
            >
              <Grid item>
                <Typography variant='body1'>Also you can...</Typography>
              </Grid>
              <Grid item>
                <Button
                  variant='outlined'
                  size='small'
                  color='default'
                  aria-label='Add'
                >
                  <Grid container alignItems='stretch' justify='space-between'>
                    <Grid item xs={8}>
                      login on
                    </Grid>
                    <Grid item xs={4}>
                      <SocialIcon
                        network='facebook'
                        style={{ height: 25, width: 25, marginLeft: 2 }}
                      />
                    </Grid>
                  </Grid>
                </Button>
              </Grid>
              <Grid item />
              <Grid item />
              <Grid item />
              <Grid item />
            </Grid>
          </Grid>
        </form>
        <Grid item xs container justify='center' zeroMinWidth>
          <Fab
            color='primary'
            style={{ position: 'absolute', bottom: '1%' }}
            onClick={this.handleSubmit}
          >
            <ArrowForwardIosIcon />
          </Fab>
        </Grid>{' '}
      </Grid>
    )
  }
}
const mapDispatchToProps = dispatch => {
  return {
    addUser: details => {
      dispatch({ type: 'addUser', details })
    }
  }
}
const mapStateToProps = state => {
  return {
    
    verified: state.auth.auth
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Signup)
