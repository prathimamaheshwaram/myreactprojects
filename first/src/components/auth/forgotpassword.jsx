import React, { Component } from 'react'
import {
  FormControl,
  InputLabel,
  Input,
  InputAdornment,
  FormHelperText,
  Grid,
  Typography,
  Fab
} from '@material-ui/core'
import MailIcon from '@material-ui/icons/Mail'
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos'
import { connect } from 'react-redux'
import Alert from './Alert'
const emailtype = RegExp(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)

class ForgotPassword extends Component {
  state = {
    email: '',
    err: '',
    alert: false,
    error: {
      email: 'enter your mail id'
    }
  }
  toggleAlert = () => {
    this.setState({ alert: !this.state.alert })
  }
  handleChange = prop => event => {
    this.setState({ [prop]: event.target.value })
    let error = this.state.error
    switch (prop) {
      case 'email':
        error.email = emailtype.test(event.target.value)
          ? ''
          : 'email should be in the form of example@example.com'
        break
      default:
        break
    }
    this.setState({ error })
  }

  handleSubmit = () => {
    if (this.formError()) {
      this.props.forgotPassword(this.state.email)
      this.setState({ err: false })
      if (this.props.verified.doesnotExist) {
        this.setState({ alert: true })
      }
    } else {
      console.log('failed')
      this.setState({ err: true })
    }
  }
  formError = () => {
    const error = Object.entries(this.state.error)
    let success = true
    error.map(e => e[1].length > 0 && (success = false))
    return success
  }

  render () {
    const { email, error, err, alert } = this.state
    return (
      <Grid container alignItems='flex-end' alignContent='center'>
        {alert && (
          <Alert
            open={alert}
            toggleAlert={this.toggleAlert}
            from='forgotpassword'
          />
        )}
        <form noValidate>
          <Grid item xs={12} container spacing={16}>
            <Grid item xs={12}>
              <Typography variant='h5' gutterBottom>
                <span className='signinbg'> Forgot Password? </span>
              </Typography>
            </Grid>
            <Grid item xs={12} />
            <Grid item xs={12}>
              <Typography variant='subtitle1'>We Will Need...</Typography>
            </Grid>

            <Grid item xs={12}>
              <FormControl fullWidth>
                <InputLabel htmlFor='email'>
                  <Typography variant='body1'>mail address</Typography>
                </InputLabel>
                <Input
                  id='email'
                  value={email}
                  onChange={this.handleChange('email')}
                  endAdornment={
                    <InputAdornment position='end'>
                      <MailIcon color='disabled' />
                    </InputAdornment>
                  }
                />
                {err && <FormHelperText error>{error.email}</FormHelperText>}
              </FormControl>
            </Grid>

            <Grid item xs={12} />
            <Grid item xs={12} />

            <Grid item xs={12}>
              <Typography variant='body1'>
                password will be sent to your mail
              </Typography>
            </Grid>
          </Grid>

          <Grid item container justify='center'>
            <Fab
              color='primary'
              style={{ position: 'absolute', bottom: '1%' }}
              onClick={this.handleSubmit}
            >
              <ArrowForwardIosIcon />
            </Fab>
          </Grid>
        </form>
      </Grid>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    forgotPassword: email => {
      dispatch({ type: 'forgotPassword', email })
    }
  }
}
const mapStateToProps = state => {
  return {
    verified: state.auth.auth
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ForgotPassword)
