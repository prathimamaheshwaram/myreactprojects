import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
// import Particle from './particles'
import { Paper, Typography, Grid, Avatar } from '@material-ui/core'
import { CSSTransition } from 'react-transition-group'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import './homescreen.css'
import logo from './logo.svg'

import Signup from '../Signup'

// const styledBy = (property, mapping) => props => mapping[props[property]]
const styles = theme => ({
  // particles: {
  //   height: '99vh'
  // },
  paper: {
    position: 'absolute',
    top: '30%',
    left: '20%',
    right: '25%',
    backgroundColor: 'rgba(0,0,0,0.6)',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: '3%',
    height: '50%'
  },
  bigAvatar: {
    width: '13vh',
    height: '13vh',
    backgroundColor: 'white'
  }
})
class SignupComponent extends Component {
  state = {
    mount: false
  }

  componentDidMount () {
    this.setState({ mount: true })
    this.props.history.location.pathname = '/sinein'
  }
  handleVerified = id => {
    this.props.history.push('/homescreen/' + id)
  }
  render () {
    const { classes } = this.props
    const { mount } = this.state
   
    const defaultContent = () => {
      return (
        <Grid
          container
          justify='flex-start'
          alignItems='flex-start'
          direction='row'
          spacing={24}
          wrap='wrap'
        >
          <Grid item xs={12}>
            <Avatar alt='Remy Sharp' src={logo} className={classes.bigAvatar} />
          </Grid>
          <Grid item xs={6}>
            <Typography variant='h6' gutterBottom>
              Welcome Back!!
            </Typography>
            <Typography variant='body2' gutterBottom>
              this is just a demo website this is just a demo website this is
              just a demo website
            </Typography>
            <Typography variant='overline' gutterBottom>
              sign in to continue >>
            </Typography>
          </Grid>
        </Grid>
      )
    }

    return (
      <div className='component'>
        <Paper className={classes.paper}>
          <CSSTransition in={mount} timeout={100} classNames='fade'>
            {defaultContent()}
          </CSSTransition>
          <Paper className='signin'>
            <Grid container>
              <Grid item>
                <Signup handleVerified={this.handleVerified} />
              </Grid>
            </Grid>
          </Paper>
        </Paper>
      </div>
    )
  }
}

SignupComponent.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(SignupComponent)
