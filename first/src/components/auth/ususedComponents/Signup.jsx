import React, { Component } from 'react'
import {
  FormControl,
  InputLabel,
  Input,
  InputAdornment,
  IconButton,
  Button,
  FormHelperText,
  Grid,
  Typography,
  Fab
} from '@material-ui/core'
import { SocialIcon } from 'react-social-icons'
import { Visibility, VisibilityOff } from '@material-ui/icons'
import MailIcon from '@material-ui/icons/Mail'
import InsertEmoticonIcon from '@material-ui/icons/InsertEmoticon'
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos'
import { connect } from 'react-redux'


const emailtype = RegExp(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)

class Signup extends Component {
  state = {
    firstName: '',
    email: '',
    password: '',
    confirmPassword: '',
    showPassword: false,
    showConfirmPassword: false,

    err: '',
    error: {
      firstName: 'enter first name',
      confirmPassword: 'confirm password',
      email: 'enter your mail id',
      password: 'enter password'
    }
  }

  handleChange = prop => event => {
    this.setState({ [prop]: event.target.value })
    let error = this.state.error
    switch (prop) {
      case 'firstName':
        error.firstName =
          event.target.value.length < 3
            ? 'first Name should be greate than 3'
            : ''
        break
      case 'confirmPassword':
        error.confirmPassword =
          event.target.value.length < 3 ? 'password should match' : ''
        break
      case 'email':
        error.email = emailtype.test(event.target.value)
          ? ''
          : 'email should be in the form of example@example.com'
        break
      case 'password':
        error.password =
          event.target.value.length < 3 ? 'enter correct password' : ''
        break
      default:
        break
    }
    this.setState({ error })
  }
  handleClickShowPassword = show => {
    this.setState(state => ({ [show]: !state[show] }))
  }
  handleSubmit = () => {
    if (
      this.formError() &
      (this.state.password === this.state.confirmPassword)
    ) {
      const {firstName,email,password}=this.state
      this.props.addUser({firstName,email,password})
      this.setState({ err: false })
    } else {
      console.log('failed')
      this.setState({ err: true })
    }
  }
  formError = () => {
    const error = Object.entries(this.state.error)
    let success = true
    error.map(e => {
      if (e[1].length > 0) {
        success = false
      }
    })
    return success
  }

  render () {
    const { confirmPassword, error, err } = this.state
    const { firstName, email, password } = this.state

    return (
      <Grid container >
        <form noValidate>
          <Grid item container spacing={16} wrap='nowrap'>
            <Grid item xs={7} container spacing={16}>
              <Grid item xs={12}>
                <Typography variant='h5' gutterBottom>
                  <span className='signinbg'> Sign Up </span>
                </Typography>
              </Grid>
              <Grid item xs={12} />
              <Grid item xs={12}>
                <Typography variant='subtitle1'>We Will Need...</Typography>
              </Grid>
              <Grid item xs={12}>
                <FormControl fullWidth>
                  <InputLabel htmlFor='firstName'>
                    <Typography variant='body1'>Your Name</Typography>
                  </InputLabel>
                  <Input
                    id='firstName'
                    value={firstName}
                    onChange={this.handleChange('firstName')}
                    endAdornment={
                      <InputAdornment position='end'>
                        <InsertEmoticonIcon color='disabled' />
                      </InputAdornment>
                    }
                  />
                  {err && (
                    <FormHelperText error>{error.firstName}</FormHelperText>
                  )}
                </FormControl>
              </Grid>

              <Grid item xs={12}>
                <FormControl fullWidth>
                  <InputLabel htmlFor='email'>
                    <Typography variant='body1'>mail address</Typography>
                  </InputLabel>
                  <Input
                    id='email'
                    value={email}
                    onChange={this.handleChange('email')}
                    endAdornment={
                      <InputAdornment position='end'>
                        <MailIcon color='disabled' />
                      </InputAdornment>
                    }
                  />
                  {err && <FormHelperText error>{error.email}</FormHelperText>}
                </FormControl>
              </Grid>

              <Grid item xs={12}>
                <FormControl fullWidth>
                  <InputLabel htmlFor='password'>
                    <Typography variant='body1'>Your Password</Typography>
                  </InputLabel>
                  <Input
                    id='adornment-password'
                    type={this.state.showPassword ? 'text' : 'password'}
                    value={password}
                    onChange={this.handleChange('password')}
                    endAdornment={
                      <InputAdornment position='end'>
                        <IconButton
                          aria-label='Toggle password visibility'
                          onClick={() =>
                            this.handleClickShowPassword('showPassword')
                          }
                        >
                          {this.state.showPassword ? (
                            <Visibility />
                          ) : (
                            <VisibilityOff />
                          )}
                        </IconButton>
                      </InputAdornment>
                    }
                  />
                  {err && (
                    <FormHelperText error>{error.password}</FormHelperText>
                  )}
                </FormControl>
              </Grid>

              <Grid item xs={12}>
                <FormControl fullWidth>
                  <InputLabel htmlFor='confirmPassword'>
                    <Typography variant='body1'>One More Time ?</Typography>
                  </InputLabel>
                  <Input
                    id='adornment-confirmPassword'
                    type={this.state.showConfirmPassword ? 'text' : 'password'}
                    value={confirmPassword}
                    onChange={this.handleChange('confirmPassword')}
                    endAdornment={
                      <InputAdornment position='end'>
                        <IconButton
                          aria-label='Toggle password visibility'
                          onClick={() =>
                            this.handleClickShowPassword('showConfirmPassword')
                          }
                        >
                          {this.state.showConfirmPassword ? (
                            <Visibility />
                          ) : (
                            <VisibilityOff />
                          )}
                        </IconButton>
                      </InputAdornment>
                    }
                  />
                  {err && (
                    <FormHelperText error>
                      {error.confirmPassword}
                    </FormHelperText>
                  )}
                </FormControl>
              </Grid>
            </Grid>
            <Grid item xs={1}>
              <hr width='1' size='155' />
              OR
              <hr width='1' size='155' />
            </Grid>
            <Grid
              item
              xs={4}
              container
              direction='column'
              wrap='nowrap'
              justify='space-around'
              alignItems='center'
            >
              <Grid item xs={12} />
              <Grid item xs={12} />
              <Grid item xs={12}>
                <Typography variant='subtitle1'>Also You Can...</Typography>
              </Grid>
              <Grid item xs={12}>
                <Grid container>
                  <Button
                    variant='outlined'
                    size='medium'
                    color='default'
                    aria-label='Add'
                  >
                    <Grid item xs={10}>
                      login on
                    </Grid>
                    <Grid item xs={2}>
                      <SocialIcon
                        network='facebook'
                        style={{ height: 25, width: 25, marginLeft: 2 }}
                      />
                    </Grid>
                  </Button>
                </Grid>
              </Grid>

              <Grid item xs={12} />
            </Grid>
          </Grid>
          <Grid item container justify='center'>
            <Fab
              color='primary'
              style={{ position: 'relative', marginTop: '2%' }}
              onClick={this.handleSubmit}
            >
              <ArrowForwardIosIcon />
            </Fab>
          </Grid>
        </form>
      </Grid>
    )
  }
}
const mapDispatchToProps = dispatch => {
  return {
    addUser: details => {
      dispatch({ type: 'addUser', details })
    }
  }
}
const mapStateToProps = state => {
  return {
    userDetails: state.auth.users
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Signup)
