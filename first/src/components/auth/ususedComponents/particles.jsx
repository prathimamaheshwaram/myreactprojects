import React from 'react'
import Particles from 'react-particles-js'
const particles = {
  particles: {
    number: {
      value: 500,
      density: {
        enable: true,
        value_area: 1500
      }
    },
    move: {
      enable: true,
      speed: 25,
      random: true,
      direction: 'none',
      bounce: true,
      out_mode: 'bounce',
      attract: {
        enable: true,
        rotateX: 10,
        rotateY: 10
      }
    },
    line_linked: {
      enable: true,
      distance: 60,
      color: '#ffffff',
      opacity: 0.5,
      width: 2
    },
    shape: {
      type: 'circle',
      opacity: {
        value: 0.6
      },
      stroke: {
        width: 5,
        color: '#ffff'
      },
      polygon: {
        nb_sides: 7
      }
    }
  },
  interactivity: {
    detect_on: 'canvas',
    events: {
      onhover: {
        enable: true,
        mode: 'repulse'
      },
      onclick: {
        enable: true,
        mode: 'push'
      },
      resize: true
    },
    modes: {
      grab: {
        distance: 400,
        line_linked: {
          opacity: 0.5
        }
      },
      bubble: {
        distance: 600,
        size: 15,
        duration: 0.2,
        opacity: 0.4
      },

      repulse: {
        distance: 100,
        duration: 0.1
      },
      push: {
        particles_nb: 4
      },
      remove: {
        particles_nb: 10
      }
    }
  },
  retina_detect: true
}
const Particle = () => {
  return <Particles params={particles} />
}

export default Particle
