import React, { Component } from 'react'
import { connect } from 'react-redux'
import Steppers from './parts/steppers'
class DashHome extends Component {
  state = {}
  render () {
    console.log(this.props.userDetails)
    return <Steppers />
  }
}
const mapStateToProps = state => {
  return {
    userDetails: state.auth.auth.details
  }
}
export default connect(mapStateToProps)(DashHome)
