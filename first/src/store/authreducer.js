const initState = {
  auth: {
    signedIn: false,
    signedUp: false,
    alreadyExist: false,
    doesnotExist: false,
    details: {
      id: 0,
      firstName: 'prathima1',
      email: 'prathimamaheshwaram1@gmail.com',
      password: '1234567'
    }
  },
  users: [
    {
      id: 0,
      firstName: 'prathima1',
      email: 'prathimamaheshwaram1@gmail.com',
      password: '1234567'
    },
    {
      id: 1,
      firstName: 'prathima2',
      email: 'prathimamaheshwaram12@gmail.com',
      password: '1234567'
    },
    {
      id: 2,
      firstName: 'prathima3',
      email: 'prathimamaheshwaram123@gmail.com',
      password: '1234567'
    }
  ],
  forgotPassword: [
    {
      email: 'prathimamaheshwaram1234@gmail.com'
    }
  ]
}
const AuthReducer = (state = initState, action) => {
  switch (action.type) {
    case 'addUser':
      {
        const repeated = state.users.filter(
          user => user.email === action.details.email
        )
        if (repeated.length === 0) {
          const id = state.users.length
          state.users[id] = { id, ...action.details, date: new Date() }
          state.auth.signedUp = true
          state.auth.details = state.users[id]
        } else {
          state.auth.alreadyExist = true
        }
      }
      break
    case 'signIn':
      {
        const user = state.users.filter(
          user =>
            action.signinDetails.email === user.email &&
            action.signinDetails.password === user.password
        )
        if (user.length !== 0) {
          state.auth.signedIn = true
          state.auth.details = user[0]
        } else {
          state.auth.signedIn = false
        }
      }
      break
    case 'forgotPassword':
      {
        const user = state.users.find(user => action.email === user.email)
        if (user === undefined) state.auth.doesnotExist = true
        console.log(user)
      }
      break
    default:
      break
  }

  return state
}

export default AuthReducer

// if (action.type === 'addUser') {
//   const repeated = state.users.filter(
//     user => user.email === action.details.email
//   )
//   if (repeated.length === 0) {
//     const id = state.users.length
//     state.users[id] = action.details
//     console.log(state.users)
//   }
// }
