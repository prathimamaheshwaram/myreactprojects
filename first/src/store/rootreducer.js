import ProjectReducer from './productreducer'
import AuthReducer from './authreducer'
import { combineReducers } from 'redux'

const rootReducer = combineReducers({
  auth: AuthReducer,
  projects: ProjectReducer
})

export default rootReducer