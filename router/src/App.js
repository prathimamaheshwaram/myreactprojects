import React, { Component } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Navbar from './routercomponents/navbar'
import Contact from './routercomponents/contact'
import About from './routercomponents/about'
import Home from './routercomponents/Home'
import Posts from './routercomponents/posts'
class App extends Component {
  state = {}
  render () {
    return (
      <BrowserRouter>
        <div>
          <Navbar />
          <Switch>
            <Route path='/' exact component={Home} />
            <Route path='/contact' component={Contact} />
            <Route path='/about' component={About} />
            <Route path='/:post_id' component={Posts} />
          </Switch>
        </div>
      </BrowserRouter>
    )
  }
}

export default App

// import React, { Component } from 'react'
// // import { BrowserRouter, Route, Switch } from 'react-router-dom'
// import './App.css'
// // import Header from './firstapp/components/header'
// // import Cart from './firstapp/components/cart'
// // import Router1 from './components/router1'
// // import Router2 from './components/router2'
// // import Router3 from './components/router3'
// // import Navigation from './components/Navigation
// import { Header, Footer } from './materialui/layout'
// import Exercise from './Exercise'
// import { muscles, exercises } from './store'
// const defaultExercise = {
//   title: 'Welcome',
//   discription: 'click any one of the execercises'
// }
// class App extends Component {
//   state = {
//     exercises,
//     catagory: 'All',
//     exercise: defaultExercise
//   }

//   sortExerciseByMuscles = () => {
//     return Object.entries(
//       this.state.exercises.reduce((exercises, exercise) => {
//         const { muscle } = exercise
//         exercises[muscle] = exercises[muscle]
//           ? [...exercises[muscle], exercise]
//           : [exercise]
//         return exercises
//       }, {})
//     )
//   }
//   handleCreateFromApp = exercise => {
//     this.setState({ exercises: [...exercises, exercise] })
//   }
//   handleCatagory = catagory => {
//     this.setState({ catagory })
//   }
//   handleSelect = id => {
//     this.state.exercises.map(element =>
//       element.id === id ? this.setState({ exercise: element }) : null
//     )
//   }
//   handleExerciseDelete = id => {
//     this.setState(({ exercises }) => ({
//       exercises: exercises.filter(element => element.id !== id)
//     }))
//   }
//   handleEditFromApp = id => {}

//   render () {
//     const exercises = this.sortExerciseByMuscles()
//     const catagory = this.state.catagory
//     return (
//       <React.Fragment>
//         <Header
//           muscles={muscles}
//           handleCreateFromApp={this.handleCreateFromApp}
//         />
//         <Exercise
//           handleEdit={id => this.handleEditFromApp(id)}
//           handleDelete={id => this.handleExerciseDelete(id)}
//           exercise={this.state.exercise}
//           exercises={exercises}
//           catagory={catagory}
//           onSelect={id => this.handleSelect(id)}
//         />

//         <Footer
//           muscles={muscles}
//           catagory={catagory}
//           onSelect={this.handleCatagory}
//         />
//       </React.Fragment>
//     )
//   }
// }

// export default App

// // <BrowserRouter>
// //         <div>
// //           <Navigation />
// //           <Switch>
// //             <Route path='/' component={Router1} exact />
// //             <Route path='/2' component={Router2} />
// //             <Route path='/3' component={Router3} />
// //           </Switch>
// //         </div>
// //       </BrowserRouter>
