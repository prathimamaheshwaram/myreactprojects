import React from 'react'
import { Grid, Paper, Typography, List } from '@material-ui/core'
import {
  ListItem,
  ListItemText,
  ListItemSecondaryAction
} from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit'
import IconButton from '@material-ui/core/IconButton'
// import LeftPane from './leftpane'
// import RightPane from './rightpane'
const Exercise = ({
  exercises,
  catagory,
  onSelect,
  exercise,
  handleDelete
}) => {
  const styles = { padding: 24, margin: 10, height: 400, overflow: 'auto' }

  return (
    <Grid container>
      <Grid item sm>
        <Paper style={styles} elevation={1}>
          {exercises.map(element => {
            return (
              <React.Fragment key={element[0]}>
                {catagory === element[0] || catagory === 'All' ? (
                  <React.Fragment>
                    <Typography
                      variant='h5'
                      style={{ textTransform: 'capitalize' }}
                    >
                      {element[0]}
                    </Typography>
                    <List component='nav'>
                      {element[1].map(({ id, title }) => {
                        return (
                          <ListItem
                            onClick={() => onSelect(id)}
                            button
                            key={title}
                          >
                            <ListItemText primary={title} />
                            <ListItemSecondaryAction>
                              <IconButton
                                aria-label='Delete'
                                onClick={() => handleDelete(id)}
                              >
                                <DeleteIcon />
                              </IconButton>
                              {/* <IconButton
                                aria-label='Edit'
                                onClick={() => handleEdit(id)}
                              >
                                <EditIcon /> */}
                              {/* </IconButton> */}
                            </ListItemSecondaryAction>
                          </ListItem>
                        )
                      })}
                    </List>
                  </React.Fragment>
                ) : null}
              </React.Fragment>
            )
          })}
        </Paper>
      </Grid>
      <Grid item sm>
        <Paper style={styles} elevation={1}>
          <Typography variant='display3'>{exercise.id}</Typography>
          <Typography style={{ margin: 15 }} variant='display1'>
            {exercise.title}
          </Typography>
          <Typography style={{ margin: 15 }} variant='display2'>
            {exercise.muscle}
          </Typography>
          <Typography style={{ margin: 15 }} variant='body1'>
            {exercise.discription}
          </Typography>
        </Paper>
      </Grid>
    </Grid>
  )
}
export default Exercise
// return (
//   <React.Fragment key={element[0]}>
//     <Typography
//       variant='h5'
//       style={{ textTransform: 'capitalize' }}
//     >
//       {element[0]}
//     </Typography>
//     <List component='nav'>
//       {element[1].map(({ title }) => {
//         return (
//           <ListItem button key={title}>
//             <ListItemText>{title}</ListItemText>
//           </ListItem>
//         )
//       })}
//     </List>
//   </React.Fragment>
// )
