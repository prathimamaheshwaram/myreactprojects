import React from 'react'
import { Paper, Typography } from '@material-ui/core'
const RightPane = () => {
  return (
    <React.Fragment>
      <Paper style={{ padding: 24, margin: 10 }} elevation={1}>
        <Typography variant='subtitle1'>RightPane</Typography>
      </Paper>
    </React.Fragment>
  )
}

export default RightPane
