import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
class Navigation extends Component {
  state = {}
  render () {
    return (
      <div>
        <NavLink to='/'>Router1</NavLink>
        <NavLink to='/2'>Router2</NavLink>
        <NavLink to='/3'>Router3</NavLink>
      </div>
    )
  }
}

export default Navigation
