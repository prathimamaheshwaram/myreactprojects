import React, { Component } from 'react'
import { Button, TextField, Dialog } from '@material-ui/core'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import Fab from '@material-ui/core/Fab'
import AddIcon from '@material-ui/icons/Add'
import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'
import { muscles } from '../../store'
class FormDialog extends Component {
  state = {
    open: false,
    exercise: {
      title: '',
      discription: '',
      muscle: ''
    }
  }

  handleClickOpen = () => {
    this.setState({ open: true })
  }

  handleClose = () => {
    this.setState({ open: false })
  }
  handleChange = name => event => {
    this.setState({
      exercise: { ...this.state.exercise, [name]: event.target.value }
    })
  }
  render () {
    const { title, discription, muscle } = this.state.exercise

    return (
      <div>
        <Fab aria-label='Add' size='small'>
          <AddIcon onClick={this.handleClickOpen} />
        </Fab>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby='fom1-dialog-title'
        >
          <DialogTitle id='fom1-dialog-title'>
            Create a New Exercise
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              please fill out the form below
            </DialogContentText>
            <form autoComplete='off'>
              <TextField
                id='standard-name'
                label='title'
                value={title}
                onChange={this.handleChange('title')}
                margin='normal'
              />
              <br />
              <TextField
                multiline
                rowsMax='4'
                label='discription'
                value={discription}
                onChange={this.handleChange('discription')}
                margin='normal'
              />
              <br />
              <FormControl>
                <InputLabel htmlFor='muscle'>muscle</InputLabel>
                <Select value={muscle} onChange={this.handleChange('muscle')}>
                  {muscles.map(muscle => (
                    <MenuItem key={muscle} value={muscle}>
                      {muscle}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </form>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={() => {
                this.handleClose()
                this.props.handleCreate(this.state.exercise)
              }}
              color='primary'
            >
              Create
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}

export default FormDialog
