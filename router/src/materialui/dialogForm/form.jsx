import React, { Component } from 'react'
import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'
import { TextField } from '@material-ui/core'
import { muscles } from '../../store'
class Form extends Component {
  render () {
    const { title, discription, muscle, handleChange } = this.props.exercise

    return (
      <React.Fragment>
        <form autoComplete='off'>
          <TextField
            id='standard-name'
            label='title'
            value={title}
            onChange={() => handleChange('title',event.target.value)}
            margin='normal'
          />
          <br />
          <TextField
            multiline
            rowsMax='4'
            label='discription'
            value={discription}
            onChange={() => handleChange('discription',event.target.value)}
            margin='normal'
          />
          <br />
          <FormControl>
            <InputLabel htmlFor='muscle'>muscle</InputLabel>
            <Select value={muscle} onChange={() => handleChange('muscle',event.target.value)}>
              {muscles.map(muscle => (
                <MenuItem key={muscle} value={muscle}>
                  {muscle}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </form>
      </React.Fragment>
    )
  }
}

export default Form
