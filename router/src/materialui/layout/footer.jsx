import React from 'react'
import { Tabs, Paper } from '@material-ui/core'
import { Tab } from '@material-ui/core'
const Footer = ({ muscles, catagory, onSelect }) => {
  const index = catagory
    ? muscles.findIndex(muscles => catagory === muscles) + 1
    : 0
  const onIndexChange = (e, index) => {
    onSelect(index === 0 ? 'All' : muscles[index - 1])
  }
  return (
    <Paper>
      <Tabs
        value={index}
        onChange={onIndexChange}
        indicatorColor='primary'
        textColor='primary'
        centered
      >
        <Tab label='All' />
        {muscles.map(muscle => (
          <Tab label={muscle} key={muscle} />
        ))}
      </Tabs>
    </Paper>
  )
}

export default Footer
