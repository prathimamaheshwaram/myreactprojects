import React from 'react'
import { AppBar, Toolbar, Typography } from '@material-ui/core'
import FormDialog from '../dialogForm/dialogform'

const Header = ({ muscles, handleCreateFromApp }) => {
  return (
    <AppBar position='static'>
      <Toolbar variant='dense'>
        <Typography variant='h6' color='inherit' style={{ flex: 1 }}>
          Exercises
        </Typography>
        <FormDialog muscles={muscles} handleCreate={handleCreateFromApp} />
      </Toolbar>
    </AppBar>
  )
}

export default Header
