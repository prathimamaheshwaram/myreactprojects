import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
// import axios from 'axios'
import { connect } from 'react-redux'

class Home extends Component {
  render () {
    const { posts } = this.props
    const postList = posts.length ? (
      posts.map(post => {
        return (
          <div className='card' key={post.id}>
            <div className='card-content'>
              <NavLink to={'/' + post.id}>
                <div className='card-title blue-text'>{post.title}</div>
              </NavLink>
              <p>{post.body}</p>
            </div>
          </div>
        )
      })
    ) : (
      <h3>Loading..</h3>
    )
    return <div className='container'>{postList}</div>
  }
}
const mapStateToProps = state => {
  return {
    posts: state.posts
  }
}

export default connect(mapStateToProps)(Home)

// state = { posts: [] }
// componentDidMount () {
//   axios.get('https://jsonplaceholder.typicode.com/posts').then(res => {
//     this.setState({ posts: res.data.slice(0, 10) })
//   })
// }
