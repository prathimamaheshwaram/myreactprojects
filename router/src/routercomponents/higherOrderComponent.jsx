import React from 'react'
const Hoc = WrappedComponent => {
  const rainbowColors = [
    'pink',
    'red',
    'yellow',
    'green',
    'black',
    'blue',
    'violet'
  ]
  const rainbowColor = rainbowColors[Math.floor(Math.random() * 7)]
  const randomColor = rainbowColor + '-text'
  return props => {
    return (
      <div className={randomColor}>
        <WrappedComponent />
      </div>
    )
  }
}

export default Hoc
