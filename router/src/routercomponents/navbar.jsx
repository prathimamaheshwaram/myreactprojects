import React from 'react'
import { NavLink, withRouter } from 'react-router-dom'

const Navbar = () => {
  // setTimeout(() => {
  //   props.history.push('/contact')
  // }, 2000)
  return (
    <nav className='nav-wrapper orange'>
      <NavLink className='brand-ligo' to='/'>
        logo
      </NavLink>
      <ul id='nav-mobile' className='right hide-on-med-and-down red-text'>
        <li>
          <NavLink to='/'>Home</NavLink>
        </li>
        <li>
          <NavLink to='/contact'>Contact</NavLink>
        </li>
        <li>
          <NavLink to='/about'>About</NavLink>
        </li>
      </ul>
    </nav>
  )
}

export default Navbar

// const Navbar = props => {
//   setTimeout(() => {
//     props.history.push('/contact')
//   }, 2000)
//   return (
//     <nav className='nav-wrapper orange'>
//       <NavLink className='brand-ligo' to='/'>
//         logo
//       </NavLink>
//       <ul id='nav-mobile' className='right hide-on-med-and-down red-text'>
//         <li>
//           <NavLink to='/'>Home</NavLink>
//         </li>
//         <li>
//           <NavLink to='/contact'>Contact</NavLink>
//         </li>
//         <li>
//           <NavLink to='/about'>About</NavLink>
//         </li>
//       </ul>
//     </nav>
//   )
// }

// export default withRouter(Navbar)
