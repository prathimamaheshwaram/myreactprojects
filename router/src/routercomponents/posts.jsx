import React, { Component } from 'react'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'

// import axios from 'axios'
class Posts extends Component {
  handleDelete = () => {
    this.props.deletePost(this.props.post.id)
  }
  render () {
    const post = this.props.post ? (
      <div>
        <h4 className='center green-text text-lighten-6'>
          {this.props.post.title}
        </h4>
        <p className='card-panel teal lighten-2'>{this.props.post.body}</p>
        <div className='center'>
          <NavLink to='/'>
            <button className='btn black' onClick={this.handleDelete}>
              Delete post
            </button>
          </NavLink>
        </div>
      </div>
    ) : (
      <div className='center'>loading...</div>
    )
    return <div className='container'>{post}</div>
  }
}
const mapStateToProps = (state, ownProps) => {
  let id = ownProps.match.params.post_id
  return {
    post: state.posts.find(post => post.id === id)
  }
}

const mapDispatchToProps = dispatch => {
  return {
    deletePost: id => dispatch({ type: 'DELETE_POST', id: id })
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Posts)

// state = { post: [] }
// componentDidMount () {
//   let id = this.props.match.params.post_id
//   axios.get('https://jsonplaceholder.typicode.com/posts/' + id).then(res => {
//     this.setState({ post: res.data })
//   })
// }
