export const muscles = ['shoulders', 'back', 'arms', 'legs', 'chest']
export const exercises = [
  {
    id: 1,
    title: 'title1',
    discription: '----discription 1',
    muscle: 'shoulders'
  },

  {
    id: 2,
    title: 'title2',
    discription: '----discription 2',
    muscle: 'back'
  },
  {
    id: 3,
    title: 'title3',
    discription: '----discription 3',
    muscle: 'arms'
  },
  {
    id: 4,
    title: 'title4',
    discription: '----discription 4',
    muscle: 'shoulders'
  },
  {
    id: 5,
    title: 'title5',
    discription: '----discription 5',
    muscle: 'legs'
  },
  {
    id: 6,
    title: 'title6',
    discription: '----discription 6',
    muscle: 'chest'
  }
]
